# terraria-apis-objects

Objects for terraria APIs.

## Installation
```
pip install terraria-apis-objects
```

## License
[GPL v3](LICENSE) © Filip K.
